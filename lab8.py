import random
from dataclasses import dataclass


@dataclass
class Dot:
    x: float = 0
    y: float = 0

    def get_x(self) -> float:
        return self.x

    def get_y(self) -> float:
        return self.y


def generate_map(amount):
    dots = []
    for _ in range(amount):
        x = random.uniform(-5, 5)
        y = random.uniform(-5, 5)
        dots.append(Dot(x, y))
    return dots


def inarea(radius, centre_dot, other_dot):
    if (other_dot.get_x() - radius <= centre_dot.get_x() <= other_dot.get_x() + radius) and (other_dot.get_y() - radius <= centre_dot.get_y() <= other_dot.get_y() + radius):
        return True
    else:
        return False


def counter(radius, centre_dot, dots):
    count = 0
    for _ in range(len(dots)):
        if inarea(radius, centre_dot, dots[_]):
            count += 1
    return count


amount = int(input('Введите количество точек на поле - '))
radius = float(input('Введите радиус области поиска - '))
dots = generate_map(amount)
centre_dot = random.choice(dots)
print('Найдено точек в указанной области - ', counter(radius, centre_dot, dots))
